function getLocalStorage() {
    var resultArr = JSON.parse(localStorage.getItem("DSUser"));
    return resultArr
}
function getEl(id) {
    return document.getElementById(id);

}
var list=getLocalStorage();
console.log("🚀 ~ file: our_teacher.js:2 ~ list", list)

function renderItem() {
    var contentHTML="";
    list.forEach(function (item) {

    if (item.loaiND){
contentHTML+=`

<div class="bt_bb_column col-xl-3 bt_bb_vertical_align_top bt_bb_align_center bt_bb_padding_text_indent bt_bb_animation_fade_in animate bt_bb_shadow_hover-inner-visible bt_bb_shape_soft-rounded animated"
    data-width="3" data-bt-override-class="{}">
    <div class="bt_bb_column_content" style="background-color:rgba(255, 255, 255, 1);">
        <div class="bt_bb_column_content_inner">
            <div class="bt_bb_image bt_bb_shape_square bt_bb_align_inherit bt_bb_hover_style_zoom-in bt_bb_content_display_always bt_bb_content_align_middle"
                data-bt-override-class="null"><span><img
                        src="${item.hinhAnh}"
                        data-image_src="${item.hinhAnh}"
                        title="teacher_1"
                        alt=${item.hinhAnh}
                        class="btLazyLoadImage btLazyLoaded"></span></div>
            <div class="bt_bb_separator bt_bb_border_style_none bt_bb_bottom_spacing_20" data-bt-override-class="null">
            </div>
            <header
                class="bt_bb_headline bt_bb_color_scheme_5 bt_bb_dash_none bt_bb_superheadline bt_bb_size_small bt_bb_align_inherit"
                style="; --primary-color:#191919; --secondary-color:#b61984;" data-bt-override-class="{}">
                <h4 class="bt_bb_headline_tag"><span class="bt_bb_headline_superheadline">${item.ngonNgu}</span><span
                        class="bt_bb_headline_content"><span>${item.hoTen}</span></span></h4>
            </header>
            <div class="bt_bb_separator bt_bb_border_style_none bt_bb_bottom_spacing_small"
                data-bt-override-class="null"></div>
            <div class="bt_bb_text">
                <p>${convertString(30,item.moTa)}</p>
            </div>
            <div class="bt_bb_separator bt_bb_border_style_none bt_bb_bottom_spacing_35" data-bt-override-class="null">
            </div>
        </div>
    </div>
</div>

`
    }
    });
    getEl("listItem").innerHTML=contentHTML;
    
    
}

function convertString(maxLength, value) {
    if (value.length > maxLength) {
      return value.slice(0, maxLength) + "...";
    } else {
      return value;
    }
  }
var t=renderItem();
console.log("🚀 ~ file: our_teacher.js:55 ~ t", t)
