// 6. Validation

function getEl(id) {
    return document.getElementById(id);

}
// Check trùng
function checkDump(idNV, nvArr) {
    var index = nvArr.findIndex(function (item) {
        return idNV == item.tk;
    })
    if (index == -1) {
        getEl("tbTaiKhoan").innerText = "";
        return true;
    } else {
        getEl("tbTaiKhoan").innerText = "Mã nhân viên bị trùng, vui lòng kiểm tra lại";
        getEl("tbTaiKhoan").style.display="block";
    }
}
// Check Họ tên
function checkName(value) {
    var reg = /^[_A-z]*((-|\s)*[_A-z])*$/;
    var isName = reg.test(value);
    if (isName) {
        getEl("tbHoTen").innerText = "";
        return true;
        
    } else {
        getEl("tbHoTen").innerText = "Tên không chứa số và ký tự đặc biệt";
        return false;
    }
}

// kiểm tra độ dài

function checkLeng(value, idErr, max, min) {
    var length = value.length;
    if (length < min || length > max) {
        document.getElementById(idErr).innerText = `Độ dài phải tử ${min} đến ${max} ký tự`;
        getEl(idErr).style.display="block";
        return false;
    } else {
        document.getElementById(idErr).innerText == '';
        return true

    }
}

// kiểm tra rỗng
function checkNull(val, idErr) {
    // var isnull = true;
    var vl = getEl(val).value;
    if (vl == "") {
        getEl(idErr).innerText = "Không được để trống trường này"
        getEl(idErr).style.display="block";
        return false;
    } else {
        getEl(idErr).innerText = "";
        return true
    }

}
// // Kiểm tra chuỗi phải là số
// function numCheck(value) {
//     const reg = /^\d+$/;
//     var isNum = reg.test(value);
//     if (isNum) {
//         getEl("tbTKNV").innerText = '';
//         return true;
//     } else {
//         getEl("tbTKNV").innerText = 'Mã nhân viên không hợp lệ!';
//         getEl("tbTKNV").style.display="block";
//         return false;
//     }
// }

// Check password 6-8 ký tự...
function checkPass(value) {
    const reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,8}$/;
    var isPass = reg.test(value);
    if (isPass) {
        getEl("tbMatKhau").innerText = '';
        return true;
    } else {
        getEl("tbMatKhau").innerText = 'Password không được để trống, có ít nhất 1 ký tự hoa, 1 ký tự đặc biệt, 1 ký tự số, độ dài 6-8!';
        getEl("tbMatKhau").style.display="block";
        return false;

    }
}
// Check email
function checkEmail(value) {
    const reg =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail=reg.test(value);
    if(isEmail){
      document.getElementById("tbEmail").innerText="";
      return true;
    } else{
      document.getElementById("tbEmail").innerText='Email không hợp lệ';
      getEl("tbEmail").style.display="block";
      return false;
    }
  }