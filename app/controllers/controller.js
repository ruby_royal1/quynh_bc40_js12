function getFormInfo() {
    var taiKhoan = getEl("TaiKhoan").value;
    var hoTen = getEl("HoTen").value;
    var matKhau = getEl("MatKhau").value;
    var email = getEl("Email").value;
    var hinhAnh = getEl("HinhAnh").value;
    var loaiND = getEl("loaiNguoiDung").value;
    var ngonNgu = getEl("loaiNgonNgu").value;
    var moTa = getEl("MoTa").value;
    var nd = {taiKhoan, hoTen, matKhau, email, loaiND, ngonNgu, moTa, hinhAnh};
    return nd;
}

function convertString(maxLength, value) {
    if (value.length > maxLength) {
      return value.slice(0, maxLength) + "...";
    } else {
      return value;
    }
  }