
var userObj = new nguoiDung();
// rút gọn lấy dữ liệu
function getEl(id) {
    return document.getElementById(id);

}


getListUser();

function getListUser() {
    userObj.layDSUser()
        .then(function (result) {
            console.log(result.data);
            renderTable(result.data);
            setLocalStorage(result.data);
        })
        .catch(function (error) {
            console.log(error);
        });
    function setLocalStorage(userArr) {
        localStorage.setItem("DSUser", JSON.stringify(userArr));
    }
}
// Gán sự kiện onclick cho button search
getEl("basic-addon2").addEventListener("click", function () {
    var mangUser = getLocalStorage();
    var searchArr = [];
    console.log(mangUser);

    var searchString = getEl("searchString").value;

    searchArr = sanPhamSer.searchUser(mangUser, searchString);

    console.log(searchArr);
    renderTable(searchArr);

});
function getLocalStorage() {
    var resultArr = JSON.parse(localStorage.getItem("DSUser"));
    return resultArr
}
// Xuất dữ liệu ra bảng
function renderTable(userArr) {
    var content = "";
    var count = 1;
    userArr.map(function (userArr, index) {
        content += `
<tr>
<td>${count}</td>
<td>${userArr.taiKhoan}</td>
<td>${userArr.matKhau}</td>
<td>${userArr.hoTen}</td>
<td>${userArr.email}</td>
<td>${userArr.ngonNgu}</td>
<td>${userArr.loaiND ? "GV" : "HV"}</td>
<td>
    <button class="btn btn-danger" onclick="xoaND('${userArr.id}')"><i class="fa fa-trash-alt"></i></button>
    <button class="btn btn-info" onclick="xemND('${userArr.id}')"><i class="fa fa-user-edit"></i></button>
</td>
</tr>
`;
        count++;
    });
    getEl("tblDanhSachNguoiDung").innerHTML = content;
};

// Validate rules
function validate() {
    isValid = true;
    var ds = getLocalStorage();
    var item = getFormInfo();
    // Check mã
    isValid = checkNull("TaiKhoan", "tbTaiKhoan") && checkDump(item.taiKhoan, ds);
    // Check tên
    isValid = isValid & checkName(item.hoTen) && checkNull("HoTen", "tbHoTen");
    // Check Mật khẩu
    isValid = isValid & checkPass(item.matKhau) && checkNull("MatKhau", "tbMatKhau");
    // Check Email
    isValid = isValid & checkEmail(item.email) && checkNull("Email", "tbEmail");
    // Check hình ảnh
    isValid = isValid & checkNull("HinhAnh", "tbHinhAnh");
    // Check Loại người dùng
    isValid = isValid & checkNull("loaiNguoiDung", "tbLoaiNguoiDung");
    // Check loại ngôn ngữ
    isValid = isValid & checkNull("loaiNgonNgu", "tbLoaiNgonNgu");
    // Check mô tả
    isValid = isValid & checkNull("MoTa", "tbMoTa") && checkLeng(item.moTa, 60, 1);

}




// Thêm USer

function addUser() {
    // Validate
    var check = validate();
    if(check){
// Lấy thông tin từ form
var nd = getFormInfo();
// Lưu vào CSDL
userObj.themUser(nd)
    .then(function (result) {

        // Tải lại danh sách khi thêm xong
        getListUser();
        // Đóng modal khi thành công
        document.querySelector("#myModal .close").click();
    })
    .catch(function (error) {
        console.log(error);
    });
    };
    

};
// Thêm nút ADD USER vào modal
var footerModal = document.querySelector(".modal-footer");
footerModal.innerHTML = `
 <button onclick="addUser()" class="btn btn-success">ADD USER</button>
 `;
// Xóa user
function xoaND(id) {
    userObj.xoaUser(id)
        .then(function (result) {
            //Load lại danh sách sau khi xóa thành công      
            getListUser();

        })
        .catch(function (error) {
            console.log(error);
        });

};

// Xem User
function xemND(id) {
    userObj.xemUser(id)
        .then(function (result) {
            console.log(result.data);
            // Mở modal cho ngta xem
            $('#myModal').modal('show');
            // Fill info vào form
            getEl("TaiKhoan").value = result.data.taiKhoan;
            getEl("HoTen").value = result.data.hoTen;
            getEl("MatKhau").value = result.data.matKhau;
            getEl("Email").value = result.data.email;
            getEl("HinhAnh").value = result.data.hinhAnh;
            getEl("loaiNguoiDung").value = result.data.loaiND;
            getEl("loaiNgonNgu").value = result.data.ngonNgu;
            getEl("MoTa").value = result.data.moTa;

            // Thêm nút cập nhật vào modal
            var footerModal = document.querySelector(".modal-footer");
            footerModal.innerHTML = `
        <button onclick="capNhatND('${result.data.id}')" class="btn btn-success">Cập nhật người dùng</button>
    `;
        })
        .catch(function (error) {
            console.log(error);
        });
};

// Cập nhật người dùng
function capNhatND(id) {
    // validate
   var check=validate();

   if(check){
// Lấy thông tin từ form
var nd = getFormInfo();
// Cập nhật thông tin mới vào Database
userObj.updateUser(id, nd)
    .then(function (result) {
        console.log(result.data);
        // Load lại list user
        getListUser();
        // Tắt modal
        document.querySelector("#myModal .close").click();
    })
    .catch(function (error) {
        console.log(error);
    });
   };
    
};

