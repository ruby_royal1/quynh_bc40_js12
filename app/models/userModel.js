const BASE_URL = "https://63b2c9a05e490925c521a5db.mockapi.io/";
function nguoiDung() {
    this.layDSUser = function () {
        // Lấy dữ liệu
        var pro = axios({
            method: 'get',
            url: `${BASE_URL}/user_manage`,

        });

        return pro;
    }
    this.themUser = function (user) {
        //POST: Thêm mới dữ liệu
        //data: dữ liệu cần thêm vào Cơ sở dữ liệu
        var pro = axios({
            method: 'post',
            url: `${BASE_URL}/user_manage`,
            data: user
        });

        return pro;
    }
    this.xoaUser = function (id) {
        //DELETE: xóa user dựa vào id
        var pro = axios({
            method: 'delete',
            url: `${BASE_URL}/user_manage/${id}`
        });

        return pro;
    }
    this.xemUser = function (id) {
        //GET: lấy data cua 1 user dựa vào id
        var pro = axios({
            method: 'get',
            url: `${BASE_URL}/user_manage/${id}`
        });

        return pro;
    }

    this.updateUser = function (id, user) {
        //PUT: cập nhật data của 1 sản phẩm dựa vào id
        var pro = axios({
            method: 'put',
            url: `${BASE_URL}/user_manage/${id}`,
            data: user
        });

        return pro;
    }

    this.searchUser = function(mangUser, chuoiTK){

        var mangTK = [];
       mangTK = mangUser.filter(function(user){
            return user.tenSP.toLowerCase().indexOf(chuoiTK.toLowerCase()) >= 0;
        });
        return mangTK;
    }
}